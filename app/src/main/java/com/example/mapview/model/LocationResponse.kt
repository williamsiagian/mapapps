package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LocationResponse {
    @SerializedName("results")
    @Expose
    var results: List<Prediction>? = null

    @SerializedName("status")
    @Expose
    var status: String? = null
}