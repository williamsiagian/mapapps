package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StructuredFormatting {
    @SerializedName("main_text")
    @Expose
    var mainText: String? = null

    @SerializedName("main_text_matched_substrings")
    @Expose
    var mainTextMatchedSubstrings: List<MainTextMatchedSubstring>? = null

    @SerializedName("secondary_text")
    @Expose
    var secondaryText: String? = null

}