package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MainTextMatchedSubstring {
    @SerializedName("length")
    @Expose
    var length: Int? = null

    @SerializedName("offset")
    @Expose
    var offset: Int? = null

}