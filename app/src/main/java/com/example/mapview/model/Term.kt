package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Term {
    @SerializedName("offset")
    @Expose
    var offset: Int? = null

    @SerializedName("value")
    @Expose
    var value: String? = null

}