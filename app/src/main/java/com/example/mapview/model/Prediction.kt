package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Prediction {
    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("matched_substrings")
    @Expose
    var matchedSubstrings: List<MatchedSubstring>? = null

    @SerializedName("place_id")
    @Expose
    var placeId: String? = null

    @SerializedName("reference")
    @Expose
    var reference: String? = null

    @SerializedName("structured_formatting")
    @Expose
    var structuredFormatting: StructuredFormatting? = null

    @SerializedName("terms")
    @Expose
    var terms: List<Term>? = null

    @SerializedName("types")
    @Expose
    var types: List<String>? = null

}