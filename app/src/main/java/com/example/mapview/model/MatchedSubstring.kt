package com.example.mapview.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MatchedSubstring {
    @SerializedName("length")
    @Expose
    var length: Int? = null

    @SerializedName("offset")
    @Expose
    var offset: Int? = null

}