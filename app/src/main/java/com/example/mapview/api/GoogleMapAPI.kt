package com.example.mapview.api

import android.graphics.Region
import com.example.mapview.model.LocationResponse
import com.example.mapview.model.PredictionResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface GoogleMapAPI {
    @GET("place/autocomplete/json")
    fun getCurrentLocation(
        @Query("latlng") input: String?,
        @Query("language") language: String?,
        @Query("region") region: String?,
        @Query("key") key: String?
    ): Call<LocationResponse?>?

    @GET("place/autocomplete/json")
    fun getPlacesAutoComplete(
        @Query("input") input: String?,
        @Query("sessiontoken") types: String?,
        @Query("radius") radius: Int?,
        @Query("language") language: String?,
        @Query("components") components: String?,
        @Query("key") key: String?
    ): Call<PredictionResponse?>?
}