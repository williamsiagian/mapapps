package com.example.mapview

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.mapview.adapter.PlacesAutoCompleteAdapter
import com.example.mapview.api.APIClient
import com.example.mapview.api.GoogleMapAPI
import com.example.mapview.model.LocationResponse
import com.example.mapview.model.Prediction
import com.example.mapview.model.PredictionResponse
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerDragListener{

    lateinit var fUsedLocationProviderClient : FusedLocationProviderClient
    lateinit var mapFragment: SupportMapFragment
    lateinit var currentLocation: Location
    lateinit var menuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_baseline_arrow_back_ios_24));
        supportActionBar?.title = "Pilih Alamat"
        toolbar.tag = "home"

        toolbar.setNavigationOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (toolbar.tag == "search"){
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportActionBar?.title = "Pilih Alamat"
                    toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_baseline_arrow_back_ios_24));
                    layoutSearch.visibility = View.GONE
                    menuItem.setVisible(true)
                    toolbar.tag = "home"
                } else{
                    finish()
                }
            }
        })

        fUsedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fetchLastLocation()
        loadData()
    }

    private fun loadData() {
        val predictions: MutableList<Prediction> = ArrayList()
        val placesAutoCompleteAdapter =
            PlacesAutoCompleteAdapter(applicationContext, predictions)
        autoCompleteTextViewPlace?.threshold = 1
        autoCompleteTextViewPlace?.setAdapter(placesAutoCompleteAdapter)
    }

    fun fetchLastLocation(){
        if (ActivityCompat.checkSelfPermission(this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        }else{
            ActivityCompat.requestPermissions(this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 10)
        }
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(){
        var task : Task<Location> = fUsedLocationProviderClient.lastLocation
        task.addOnSuccessListener(object : OnSuccessListener<Location> {
            override fun onSuccess(location: Location?) {
                if (location != null) {
                    currentLocation = location
                    mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
                    mapFragment?.getMapAsync(this@MainActivity)
                }
            }

        })
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.apply {
            var latLng = currentLocation?.longitude?.let { LatLng(currentLocation?.latitude, it) }
            var options = latLng?.let { MarkerOptions().position(it).title("I am Here").draggable(true) }
            animateCamera(CameraUpdateFactory.newLatLng(latLng))
            animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F))
            addMarker(options)
            getCompleteAddress(currentLocation?.latitude, currentLocation?.longitude)
            setOnMarkerDragListener(this@MainActivity)
        }
    }

    fun getCompleteAddress(latitude: Double, longitude: Double){
        var geocoder: Geocoder?
        var addresses: List<Address>?

        geocoder = Geocoder(this, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        val address = addresses[0]
            .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        val street = addresses[0].thoroughfare
        val numberOfStreet = addresses[0].subThoroughfare

        tvAddressName.text = "$street $numberOfStreet"
        tvAddressDetail.text = address
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 10){
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        menuItem = item
        when (item.itemId) {
            R.id.search_badge -> {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_baseline_close_24));
                supportActionBar?.title = "Cari Alamat"
                toolbar.tag = "search"
                layoutSearch.visibility = View.VISIBLE
                item.setVisible(false)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMarkerDragEnd(marker: Marker?) {
        var latLng: LatLng? = marker?.position
        try {
            btn.setOnClickListener {
                latLng?.latitude?.let { it1 -> getCompleteAddress(it1, latLng?.longitude) }
            }
        }catch (e: IOException){
            e.printStackTrace()
        }
    }

    override fun onMarkerDragStart(p0: Marker?) {
        Log.d(TAG, "onMarkerDragStart: ")
    }

    override fun onMarkerDrag(p0: Marker?) {
        Log.d(TAG, "onMarkerDrag: ")
    }

    companion object{
        var TAG : String? = "MainActivity"
    }
}