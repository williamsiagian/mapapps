package com.example.mapview.adapter

import android.content.Context
import android.provider.Settings.Secure
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.Nullable
import com.example.mapview.R
import com.example.mapview.api.APIClient
import com.example.mapview.api.GoogleMapAPI
import com.example.mapview.model.Prediction
import com.example.mapview.model.PredictionResponse


class PlacesAutoCompleteAdapter(
    context: Context,
    predictions: MutableList<Prediction>?
) :
    ArrayAdapter<Prediction?>(context, R.layout.place_row_layout,
        predictions as List<Prediction?>
    ) {
    private val predictions: MutableList<Prediction>?
    override fun getView(
        position: Int,
        @Nullable convertView: View?,
        parent: ViewGroup
    ): View {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.place_row_layout, null)
        if (predictions != null && predictions.size > 0) {
            val prediction: Prediction = predictions[position]
            val textViewName = view.findViewById<TextView>(R.id.tvAddressName)
            textViewName.setText(prediction.description)
        }
        return view
    }
    private val android_id = Secure.getString(
        getContext().contentResolver,
        Secure.ANDROID_ID
    )

    override fun getFilter(): Filter {
        return PlacesAutoCompleteFilter(this, context)
    }

    private inner class PlacesAutoCompleteFilter(
        private val placesAutoCompleteAdapter: PlacesAutoCompleteAdapter,
        private val context: Context
    ) :
        Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults? {
            return try {
                placesAutoCompleteAdapter.predictions?.clear()
                val filterResults = FilterResults()
                if (charSequence == null || charSequence.length == 0) {
                    filterResults.values = ArrayList<Prediction>()
                    filterResults.count = 0
                } else {
                    val googleMapAPI: GoogleMapAPI =
                        APIClient.client?.create(GoogleMapAPI::class.java)!!
                    val predictions: PredictionResponse? = googleMapAPI.getPlacesAutoComplete(
                        charSequence.toString(),
                        android_id,
                        1,
                        "id",
                        "country%3Aid",
                        context.getString(R.string.map_key)
                    )?.execute()?.body()
                    filterResults.values = predictions?.predictions
                    filterResults.count = predictions?.predictions?.size!!
                }
                filterResults
            } catch (e: Exception) {
                null
            }
        }

        override fun publishResults(
            charSequence: CharSequence?,
            filterResults: FilterResults?
        ) {
            placesAutoCompleteAdapter?.predictions?.clear()
            placesAutoCompleteAdapter?.predictions?.addAll((filterResults?.values as? List<Prediction>)!!)
            placesAutoCompleteAdapter?.notifyDataSetChanged()
        }

        override fun convertResultToString(resultValue: Any): CharSequence {
            val prediction: Prediction = resultValue as Prediction
            return prediction.description!!
        }

    }

    init {
        this.predictions = predictions
    }
}